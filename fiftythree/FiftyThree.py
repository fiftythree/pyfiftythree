import requests
import json
from importlib import resources
from jsonschema import validate, ValidationError
from .error import SchemaValidationError
from . import schema


class FiftyThree:
    """FiftyThree client for simpler python workability.
    """

    def __init__(self, api_key: str):
        """Initializes the FiftyThree client.

        Args:
            api_key (str): 
        """
        self.api_key = api_key
        self.base_url = "http://localhost:3001/api"  ## need to update after API available
        self.headers = {
            "Authorization": f"Bearer {self.api_key}",
            "Content-Type": "application/json"
        }
        self.pred_schema = self._load_pred_schema()

    def _load_pred_schema(self):
        sch = resources.files(schema) / "prediction-schema.json"

        with open(sch, 'r') as fn:
            pred_schema = json.load(fn)
        return pred_schema

    def validate_prediction_local(self, input_data: dict) -> None:
        """For basic local testing of schema, before testing remote

        Args:
            input_data (dict): prediction data per schema

        Raises:
            SchemaValidationError: schema doesn't match prediction-schema.json
        """
        try:
            validate(instance=input_data, schema=self.pred_schema)
        except ValidationError as e:
            raise SchemaValidationError(e)

    def send_prediction(self, prediction_data: dict) -> dict:
        """
        Sends prediction data to the Next.js app's API.

        :param prediction_data: The data for the prediction to be sent.
        :return: Response from the API.
        """
        response = requests.post(f"{self.base_url}/predictions", 
                                 json=prediction_data, headers=self.headers)
        response.raise_for_status()  # Raises an exception for any response error
        return response.json()

    def get_predictions_for_model(self, model_id: str) -> dict:
        """
        Retrieves predictions for a specified model.

        :param model_id: The ID of the model for which predictions are to be retrieved.
        :return: Predictions data for the specified model.
        """
        response = requests.get(f"{self.base_url}/predictions?model={model_id}", headers=self.headers)
        response.raise_for_status()
        return response.json()

    def get_models(self) -> dict:
        """
        Retrieves all available models for a user.

        :return: List of models.
        """
        response = requests.get(f"{self.base_url}/models", headers=self.headers)
        response.raise_for_status()
        return {"models": response.text}


# You can further expand the class by adding more methods for other endpoints or functionalities.


## Note:
## 1. Ensure you have the `requests` library installed (`pip install requests`).
## 2. Update the `base_url` default value to your actual domain where the Next.js API is hosted.
## 3. Make sure to handle potential exceptions (e.g., network issues, invalid API keys, etc.) appropriately based on your application's needs.