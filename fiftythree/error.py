class FiftyThreeError(Exception):
    """Base exception class for FiftyThree client."""
    pass


class APIError(FiftyThreeError):
    """Generic API error."""
    pass


class APIConnectionError(APIError):
    """API connection error."""
    pass


class APIResponseError(APIError):
    """Unexpected API response error."""
    pass


class SchemaValidationError(FiftyThreeError):
    """Schema validation error."""
    pass


class AuthenticationError(FiftyThreeError):
    """Authentication-related error."""
    pass


class RateLimitError(FiftyThreeError):
    """Rate limit exceeded error."""
    pass


class ResourceNotFoundError(FiftyThreeError):
    """Requested resource was not found."""
    pass


class InvalidConfigurationError(FiftyThreeError):
    """Client configuration error."""
    pass


class ResourceConflictError(FiftyThreeError):
    """Resource conflict error, e.g., creating an already existing resource."""
    pass


class DeprecationError(FiftyThreeError):
    """Deprecation warning for outdated methods or features."""
    pass
