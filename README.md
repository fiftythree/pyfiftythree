# FiftyThree Client Library

The `fiftythree` Python client library provides an easy-to-use interface to interact with the FiftyThree Next.js application's API, enabling users to send and retrieve predictions programmatically.

## Installation

To install the `fiftythree` client library, use pip:

```bash
pip install fiftythree python-dotenv
```

## Usage

After installation, you can use the FiftyThree client to send predictions and perform other related tasks. Below is a basic usage example:

```python
from fiftythree import FiftyThree
from dotenv import load_dotenv

# Initialize the client with your API key
load_dotenv()
api_key = os.environ.get('FIFTYTHREE_APIKEY')
client_53 = FiftyThree(api_key)

# Example data to send a prediction
model_output = {
    "prediction": "over",
    "predictionConf": 62,
    "betValue": 85
}

# Additional data, e.g., model input, can be added to the data to send
output_to_send = model_input | model_output

# Send the prediction
response_53 = client_53.send_prediction(output_to_send)

# The response will contain relevant information, e.g., the newly issued prediction ID
print(response_53)
```

## Contributing

If you wish to contribute to this library, please fork the repository, make your changes, and submit a merge request. We value your input!

## Test

To run tests, just run `pytest` from the root directory.

## License

MIT License

## Support

For any questions or issues, please open an issue [here](https://gitlab.com/fiftythree/pyfiftythree/-/issues) or reach out to <support@fiftythree.com>
