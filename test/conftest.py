import pytest
from fiftythree import FiftyThree
from dotenv import load_dotenv
import os
from pathlib import Path


@pytest.fixture
def client() -> FiftyThree:
    """A dummy api_key for testing purposes."""
    load_dotenv(dotenv_path=Path(__file__).absolute().parent)
    API_KEY = os.environ.get("FIFTYTHREE_APIKEY")
    return FiftyThree(API_KEY)


@pytest.fixture
def sample_prediction_succ_1() -> dict[str, str | int]:
    return {
        "model": "model-1",
        "league": "us-mlb",
        "competH": "pit",
        "competA": "bal",
        "gameday": 20230505,
        "platform": "fanduel",
        "odds": "over-under",
        "line": 3,
        "value": -120,
        "prediction": "over",
        "predictionConf": 53,
        "betValue": 220
    }


@pytest.fixture
def perturb_pred_data(sample_prediction_succ_1):
    """Update successful input with some k,v pair (e.g. of bad data)

    Args:
        sample_prediction_succ_1 (pytest.fixture): a good prediction

    Note: this is a factory function.
    """
    def _perturb_pred_data(k, v) -> dict[str, str | int]:
        sample_pred = dict(sample_prediction_succ_1)
        sample_pred[k] = v
        return sample_pred

    return _perturb_pred_data
