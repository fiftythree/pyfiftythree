import pytest
from fiftythree import FiftyThree
from fiftythree.error import SchemaValidationError


def test_validate_input_success(client: FiftyThree,
                                sample_prediction_succ_1):
    assert client.validate_prediction_local(sample_prediction_succ_1) is None


def test_validate_input_failure_missing_field(client: FiftyThree,
                                              sample_prediction_succ_1):
    """Reasons for failure:

    Check that the expected fields are in the schema
    """
    necessary_keys_to_remove = [
        'gameday',
        'model',
    ]

    unnecessary_keys_to_remove = [
        'betValue',
    ]

    for necess_key in necessary_keys_to_remove:
        should_fail_pred = dict(sample_prediction_succ_1)
        should_fail_pred.pop(necess_key)
        with pytest.raises(SchemaValidationError):
            client.validate_prediction_local(input_data=should_fail_pred)

    for unnecess_key in unnecessary_keys_to_remove:
        should_succ_pred = dict(sample_prediction_succ_1)
        should_succ_pred.pop(unnecess_key)
        client.validate_prediction_local(input_data=should_succ_pred)


def test_validate_input_failure_wrong_type(client: FiftyThree,
                                           perturb_pred_data):
    """Reasons for failure:

    Gameday value is a string. According to the schema, it should be an integer
    """

    wrong_values = [
        ("gameday", "20230105"),
     ]

    for k, v in wrong_values:
        test_pred = perturb_pred_data(k, v)
        with pytest.raises(SchemaValidationError):
            client.validate_prediction_local(input_data=test_pred)


def test_validate_input_failure_wrong_values(client: FiftyThree,
                                             perturb_pred_data):
    """Reasons for failure:

    The prediction-conf value exceeds the maximum allowed value of 100.
    The bet-value exceeds the maximum allowed value of 10000.
    The prediction value "upward" is not in the enumerated list of allowed values (i.e., "over", "under", "home", "away").
    """

    wrong_values = [
        ("prediction", "upward"),
        ("betValue", -120),
        ("betValue", 12000),
    ]

    for k, v in wrong_values:
        test_pred = perturb_pred_data(k, v)
        with pytest.raises(SchemaValidationError):
            client.validate_prediction_local(input_data=test_pred)
