from unittest.mock import patch
import requests


@patch("requests.post")
def test_send_prediction(mock_post, client, sample_prediction_succ_1):
    # Create a mock response object
    mock_response = requests.Response()
    mock_response.status_code = 200
    mock_response.json = lambda: {"prediction_id": "12345"}

    # Set the mock response for the mocked post method
    mock_post.return_value = mock_response

    response = client.send_prediction(sample_prediction_succ_1)

    assert response == {"prediction_id": "12345"}


def test_get_models(client):
    models = client.get_models()
    assert models is not None
    assert isinstance(models, dict)
